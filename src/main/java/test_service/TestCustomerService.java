/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author sTCFILMs
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0889991111"));
        Customer cus1 = new Customer("Film","0955976647");
        cs.addNew(cus1);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0955976647");
        delCus.setTel("0891482246");
        cs.delete(delCus);
        cs.update(delCus);
        System.out.println("After Update");
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete(delCus);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
